import * as THREE from 'three';
import TWEEN from '@tweenjs/tween.js';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js';

export default class World {
  constructor({ element, debug = false }) {
    this.camera = new THREE.PerspectiveCamera( 25, window.innerWidth / window.innerHeight, 1, 200 );
    this.camera.position.set(40, 0, 0);
    this.camera.lookAt(0, 0, 0);

    this.scene = new THREE.Scene();
    this.scene.background = new THREE.Color( 0x333333 );

    const light = new THREE.AmbientLight(parseInt ( '#ffffff'.replace("#","0x"), 16 ), 0.5);
    this.scene.add(light);

    const directionalLight = new THREE.DirectionalLight( 0xffffff );
    directionalLight.position.set( 0, 1, 1 ).normalize();
    this.scene.add( directionalLight );	

    this.renderer = new THREE.WebGLRenderer( { antialias: true } );
    this.renderer.setSize( window.innerWidth, window.innerHeight );

    if (debug) {
      this.controls = new OrbitControls( this.camera, this.renderer.domElement );
      this.scene.add(new THREE.AxesHelper(5));
    }

    element.appendChild( this.renderer.domElement );

    this.renderer.render( this.scene, this.camera );

    this.mixers = [];
    this.clocks = [];
  }

  animateCameraTo(x, y, z, duration) {
    const currentPosition = this.camera.position;

    new TWEEN.Tween( currentPosition ).to( {
      x: x,
      y: y,
      z: z}, duration )
    .easing( TWEEN.Easing.Linear.None ).start();
  }

  start() {
    const animate = (() => {
      requestAnimationFrame( animate );
      TWEEN.update();
      this.camera.lookAt( 0, 0, 0 );
      this.mixers.forEach((mixer, index) => {
        mixer.update(this.clocks[index].getDelta());
      });
      this.renderer.render( this.scene, this.camera );
    })
    animate();
  } 
}