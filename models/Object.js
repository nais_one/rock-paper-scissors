import * as THREE from 'three';
import TWEEN from '@tweenjs/tween.js';

import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';

export default class Object {
  constructor({ object, world }) {
    this.world = world;
    this.objectName = object;
  }
  
  async init() {
    return new Promise(resolve => {
      const loader = new GLTFLoader();
      loader.load(
        // resource URL
        '/' + this.objectName,
        // called when the resource is loaded
        ( gltf ) => {
          const mesh = gltf.scene.children[0];
          this.mesh = mesh;
          this.mesh.material = new THREE.MeshNormalMaterial();
          this.mixer = new THREE.AnimationMixer(this.mesh);
          const animations = gltf.animations;
          this.animations = {};
          animations.forEach(animation => {
            this.animations[animation.name] = this.mixer.clipAction(animation);
          });

          this.wrapper = new THREE.Group();
          this.wrapper.add(this.mesh);
          this.world.scene.add(this.wrapper);
          this.world.clocks.push(new THREE.Clock());
          this.world.mixers.push(this.mixer);
          resolve();
        }
      );
    });
  }

  animateToPosition({ x, y, z, duration }) {
    new TWEEN.Tween( this.wrapper.position ).to( {
      x: x,
      y: y,
      z: z}, duration )
    .easing( TWEEN.Easing.Cubic.InOut ).start();
  }

  setPosition({ x, y, z }) {
    this.wrapper.position.set(x, y, z);
    return this;
  }
  
  animate(animationName, animationDuration = 1, looping = false) {
    const loopType = looping ? THREE.LoopPingPong : THREE.LoopOnce;
    this.previousAnimation = this.activeAnimation
    this.activeAnimation = animationName;

    const animation = this.animations[animationName];
    animation.setLoop(loopType);
    animation.setDuration(animationDuration);
    animation.clampWhenFinished = true;
    this.mixer.stopAllAction();
    animation.play();
    if (this.previousAnimation !== undefined) {
      animation.crossFadeFrom(this.animations[this.previousAnimation], animationDuration);
    }

    return this;
  }

  rotate({ axis, amount }) {
    let axisVector;

    switch (axis.toLowerCase()) {
      case 'x':
        axisVector = new THREE.Vector3(1, 0, 0);
        break;
      case 'y':
        axisVector = new THREE.Vector3(0, 1, 0);
        break;
      case 'z':
        axisVector = new THREE.Vector3(0, 0, 1);
        break;
      default:
        console.error('Not a valid axis. Valid axisNames are: x, y, z');
        break;
    }

    this.wrapper.rotateOnWorldAxis(axisVector, THREE.Math.degToRad(amount));

    return this;
  }
}