import Object from './Object';

export default class Player extends Object {    
  constructor(world) {
    super({
      object: 'hand.glb',
      world,
    });
  }
}