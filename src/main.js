import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import { library } from '@fortawesome/fontawesome-svg-core'
import { faHandRock, faHandScissors, faHandPaper } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(faHandRock, faHandScissors, faHandPaper);

Vue.component('font-awesome-icon', FontAwesomeIcon)


import VueSocketio from 'vue-socket.io';
Vue.use(new VueSocketio({
  debug: true,
  connection: 'localhost:3000',
  vuex: {
    store,
    actionPrefix: 'SOCKET_',
    mutationPrefix: 'SOCKET_'
  },
}))

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: function (h) { return h(App) }
}).$mount('#app')
